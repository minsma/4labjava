# 4 Duomenų Struktūrų su Java Labaratorinis darbas #

1.	Pakete lab4pavardė sudarykite individualių elementų atvaizdžių panaudojimo klasę, kurioje būtų atvaizdžio formavimas, poros šalinimas atvaizdyje, raktą atitinkančios reikšmės paieška ir pan., panaudojant klasę MapKTU (3 metodai). Sukurtų metodų veikimą demonstruokite pateiktuose Swing arba JavaFX dialoguose arba sukurkite nuosavą, pasinaudodami paskaitų medžiaga.  
2.	Klasėje MapKTU realizuokite metodą boolean containsValue(Value value), patikrinantį ar atvaizdyje egzistuoja duota reikšmė, ir metodus, skaičiuojančius maišos lentelės charakteristikas: vidutinį grandinėlės ilgį ir maišos lentelės masyvo tuščių elementų kiekį.  
3.	Pakete lab4pavardė sukurkite MapADT interfeisą maišos lentele realizuojančią klasę MapKTUOA, kolizijas sprendžiant vienu atviros adresacijos metodu: tiesinio dėstymo, kvadratinio dėstymo ar dvigubos maišos.  
4.	Ištirkite ir su MapKTU realizacija palyginkite 4 punkte sukurtos MapADT realizacijos metodų greitaveiką, sudarykite vykdymo laikų grafikus ir atlikite rezultatų analizę. Tai atlikite su Jūsų individualios klasės atvaizdžių poromis ir su faile zodynas.txt esančiais žodžiais (tyrimui užtenka, kad raktas ir reikšmė būtų tas pats žodis).  
  
Invidualūs metodai:  
int	getNumberOfCollisions  
V	putIfAbsent(K key, V value)

