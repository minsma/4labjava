package lab4smalinskas;

import java.util.Locale;
import laborai.studijosktu.HashType;
import laborai.studijosktu.Ks;
import laborai.studijosktu.MapKTUx;

public class TelefonuTestai {
    
    public static void main(String[] args){
        Locale.setDefault(Locale.US);
        //atvaizdzioTestas();
        greitaveikosTestas();
    }
    
    public static void atvaizdzioTestas(){
        Telefonas t1 = new Telefonas("Iphone", "7", 2015, 660.9);
        Telefonas t2 = new Telefonas("LG", "G5", 2016, 500);
        Telefonas t3 = new Telefonas("Samsung Galaxy", "J7", 2016, 400);
        Telefonas t4 = new Telefonas("Iphone", "6", 2014, 600);
        Telefonas t5 = new Telefonas("Iphone", "X", 2017, 1260);
        Telefonas t6 = new Telefonas("Iphone", "7s", 2016, 800);
        Telefonas t7 = new Telefonas("Iphone", "5s", 2014, 450);
        Telefonas t9 = new Telefonas("Huawei", "X", 2013, 250);
        
        // Raktu masyvas
        String[] telefonuId = {"NA150", "NA170", "NA160", "NA168", "NA195", 
            "NA104", "NA106", "NA105", "NA108", "NA109", "NA110"};
        int id = 0;
        MapKTUx<String, Telefonas> atvaizdis =
                new MapKTUx(new String(), new Telefonas(), HashType.DIVISION);
        
        // reiksmiu masyvas
        Telefonas[] telefonai = {t1, t2, t3, t4, t5, t6, t7, t1, t2, t2};
        for(Telefonas t : telefonai){
            atvaizdis.put(telefonuId[id++], t);
        }
        
        atvaizdis.println("Poru issidestymas atvaizdyje pagal raktus");
        Ks.oun("Ar egzistuoja pora atvaizdyje");
        Ks.oun(atvaizdis.contains(telefonuId[6]));
        Ks.oun(atvaizdis.contains(telefonuId[1]));
        Ks.oun("Pasalinamos poros is atvaizdzio:");
        Ks.oun(atvaizdis.remove(telefonuId[1]));
        Ks.oun(atvaizdis.remove(telefonuId[2]));
        atvaizdis.println("Poru issidestymas atvaizdyje pagal raktus");
        Ks.oun("Atliekame poru paieska atvaizdyje:");
        Ks.oun(atvaizdis.get(telefonuId[2]));
        Ks.oun(atvaizdis.get(telefonuId[7]));
        Ks.oun("Isspausdiname atvaizdzio poras String eilute:");
        Ks.ounn(atvaizdis);
        Ks.oun("containsValue bandymas:");
        Ks.oun(atvaizdis.containsValue(t2));
        Ks.oun(atvaizdis.containsValue(t9));
        Ks.oun("averageChainSize bandymas:");
        Ks.oun(atvaizdis.averageChainSize());
        Ks.oun("numberOfEmpties bandymas:");
        Ks.oun(atvaizdis.numberOfEmpties());
        Ks.oun("putIfAbsent bandymas:");
        Ks.oun(atvaizdis.putIfAbsent("NA250", t9));
        Ks.oun(atvaizdis.putIfAbsent("NA250", t9));
        atvaizdis.println("Poru issidestymas atvaizdyje pagal raktus");
        Ks.oun("getNumberOfCollisions bandymas:");
        Ks.oun(atvaizdis.getNumberOfCollisions());
    }
    
    // Konsoliniame rezime
    private static void greitaveikosTestas(){
        System.out.println("Greitaveikos tyrimas:\n");
        GreitaveikosTyrimas gt = new GreitaveikosTyrimas();
        //Šioje gijoje atliekamas greitaveikos tyrimas
        new Thread(() -> gt.pradetiTyrima(),
                "Greitaveikos_tyrimo_gija").start();
        try {
            String result;
            while (!(result = gt.getResultsLogger().take())
                    .equals(GreitaveikosTyrimas.FINISH_COMMAND)) {
                System.out.println(result);
                gt.getSemaphore().release();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        gt.getSemaphore().release();
    }
}
