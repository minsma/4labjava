package lab4smalinskas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import laborai.studijosktu.Ks;
import laborai.studijosktu.MapADT;


public class MapKTUOAx<K, V> extends MapKTUOA<K, V> implements MapADT<K, V>{
    
    private K baseKey;      // Bazinis objektas skirtas nauju kurimui
    private V baseObj;      // Bazinis objektas skirtas nauju kurimui
    
    public MapKTUOAx(K baseKey, V baseObj) {
        this(baseKey, baseObj, DEFAULT_INITIAL_CAPACITY);
    }
    
    public MapKTUOAx(K baseKey, V baseObj, int initialCapacity) {
        this(baseKey, baseObj, initialCapacity, DEFAULT_LOAD_FACTOR);
    }
    
    public MapKTUOAx(K baseKey, V baseObj, int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
        this.baseKey = baseKey;     // fiksacija dėl naujų elementų kūrimo
        this.baseObj = baseObj;     // fiksacija dėl naujų elementų kūrimo
    }
    
    public V put(String dataString) {
        return put((K) dataString, (V) dataString);
    }
    
    public void load(String filePath) {
        clear();
        if (filePath == null || filePath.length() == 0) {
            return;
        }
        if ((baseKey == null) || (baseObj == null)) { // Elementų kūrimui reikalingas baseObj
            Ks.ern("Naudojant load-metodą, "
                    + "reikia taikyti konstruktorių = new MapKTU(new Data(),new Data())");
        }
        try {
            try (BufferedReader fReader = Files.newBufferedReader(Paths.get(filePath), Charset.defaultCharset())) {
                fReader.lines().filter(line -> line != null && !line.isEmpty()).forEach(line -> put(line));
            }
        } catch (FileNotFoundException e) {
            Ks.ern("Tinkamas duomenų failas " + filePath + " nerastas");
        } catch (IOException | UncheckedIOException e) {
            Ks.ern("Failo " + filePath + " skaitymo klaida:" + e.getLocalizedMessage());
        }
    }
    
    public void save(String filePath) {
        throw new UnsupportedOperationException("save nepalaikomas");
    }
    
    public void println() {
        if (super.isEmpty()) {
            Ks.oun("Atvaizdis yra tuščias");
        } else {
            String[][] data = getModelList("=");
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data[i].length; j++) {
                    String format = (j == 0 | j % 2 == 1) ? "%7s" : "%15s";
                    Object value = data[i][j];
                    Ks.ouf(format, (value == null ? "" : value));
                }
                Ks.oufln("");
            }
        }

        Ks.oufln("****** Bendras porų kiekis yra " + super.size());
    }
    
    public void println(String title) {
        Ks.ounn("========" + title + "=======");
        println();
        Ks.ounn("======== Atvaizdžio pabaiga =======");
    }
    
    public String[][] getModelList(String delimiter) {
        String[][] result = new String[table.length][];
        int count = 0;
        
        for (Entry<K, V> n : table) {
            List<String> list = new ArrayList();
            
            list.add( "[ " + count + " ]");
            
            if(n != null){
                list.add("-->");
                list.add(split(n.toString(), delimiter));
            }
            
            result[count] = list.toArray(new String[0]);
            count++;
        }
        
        return result;
    }

    private String split(String s, String delimiter) {
        int k = s.indexOf(delimiter);
        if (k <= 0) {
            return s;
        }
        return s.substring(0, k);
    }
}
