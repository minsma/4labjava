package lab4smalinskas;

import laborai.gui.swing.Lab4Window;
import java.util.Locale;

/*
 * Darbo atlikimo tvarka - čia yra pradinė klasė.
 */
public class VykdymoModulis {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US); // Suvienodiname skaičių formatus
        TelefonuTestai.atvaizdzioTestas();
        Lab4Window.createAndShowGUI();
    }
}