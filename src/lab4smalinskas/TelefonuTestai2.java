package lab4smalinskas;

import java.util.Locale;
import laborai.studijosktu.Ks;

public class TelefonuTestai2 {
    public static void main(String[] args){
        Locale.setDefault(Locale.US);
        atvaizdzioTestas();
        greitaveikosTestas();
    }
    
    public static void atvaizdzioTestas(){
        Telefonas t1 = new Telefonas("Iphone", "7", 2015, 660.9);
        Telefonas t2 = new Telefonas("LG", "G5", 2016, 500);
        Telefonas t3 = new Telefonas("Samsung Galaxy", "J7", 2016, 400);
        Telefonas t4 = new Telefonas("Iphone", "6", 2014, 600);
        Telefonas t5 = new Telefonas("Iphone", "X", 2017, 1260);
        Telefonas t6 = new Telefonas("Iphone", "7s", 2016, 800);
        Telefonas t7 = new Telefonas("Iphone", "5s", 2014, 450);
        Telefonas t9 = new Telefonas("Huawei", "X", 2013, 250);
        
        // Raktu masyvas
        String[] telefonuId = {"NA150", "NA170", "NA160", "NA168", "NA195", 
            "NA104", "NA106", "NA105", "NA108", "NA109", "NA110"};
        int id = 0;
        MapKTUOAx<String, Telefonas> atvaizdis =
                new MapKTUOAx(new String(), new Telefonas());
        
        // reiksmiu masyvas
        Telefonas[] telefonai = {t1, t2, t3, t4, t5, t6, t7, t1, t2, t2};
        for(Telefonas t : telefonai){
            atvaizdis.put(telefonuId[id++], t);
        }
        
        atvaizdis.println("Poru issidestymas atvaizdyje pagal raktus");
        Ks.oun("Ar egzistuoja pora atvaizdyje");
        Ks.oun(atvaizdis.contains(telefonuId[6]));
        Ks.oun(atvaizdis.contains(telefonuId[1]));
        Ks.oun("Pasalinamos poros is atvaizdzio:");
        Ks.oun(atvaizdis.remove(telefonuId[1]));
        Ks.oun(atvaizdis.remove(telefonuId[2]));
        atvaizdis.println("Poru issidestymas atvaizdyje pagal raktus");
        Ks.oun("Atliekame poru paieska atvaizdyje:");
        Ks.oun(atvaizdis.get(telefonuId[3]));
        Ks.oun(atvaizdis.get(telefonuId[7]));
        Ks.oun("Isspausdiname atvaizdzio poras String eilute:");
        Ks.ounn(atvaizdis);
    }
    
    // Konsoliniame rezime
    private static void greitaveikosTestas(){
        System.out.println("Greitaveikos tyrimas:\n");
        GreitaveikosTyrimas gt = new GreitaveikosTyrimas();
        //Šioje gijoje atliekamas greitaveikos tyrimas
        new Thread(() -> gt.pradetiTyrima(),
                "Greitaveikos_tyrimo_gija").start();
        try {
            String result;
            while (!(result = gt.getResultsLogger().take())
                    .equals(GreitaveikosTyrimas.FINISH_COMMAND)) {
                System.out.println(result);
                gt.getSemaphore().release();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        gt.getSemaphore().release();
    }
}
