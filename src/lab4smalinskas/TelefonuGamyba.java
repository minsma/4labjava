package lab4smalinskas;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.IntStream;
import laborai.gui.MyException;

public class TelefonuGamyba {
    
    private static final String ID_CODE = "NA";
    private static int serNr = 10000;
    
    private Telefonas[] telefonai;
    private String[] raktai;
    private int kiekis = 0, idKiekis = 0;
    
    public static Telefonas[] gamintiTelefonus(int kiekis){
        Telefonas[] telefonai = IntStream.range(0, kiekis)
                .mapToObj(i -> new Telefonas.Builder().buildRandom())
                .toArray(Telefonas[]::new);
        
        Collections.shuffle(Arrays.asList(telefonai));
        
        return telefonai;
    }
    
    public static String[] gamintiTelefonuIds(int kiekis){
        String[] raktai = IntStream.range(0, kiekis)
                .mapToObj(i -> ID_CODE + (serNr++))
                .toArray(String[]::new);
        
        Collections.shuffle(Arrays.asList(raktai));
        
        return raktai;
    }
    
    public Telefonas[] gamintiIrParduotiTelefonus(int aibesDydis,
            int aibesImtis) throws MyException {
        if(aibesImtis > aibesDydis){
            aibesImtis = aibesDydis;
        }
        
        telefonai = gamintiTelefonus(aibesDydis);
        raktai = gamintiTelefonuIds(aibesDydis);
        this.kiekis = aibesImtis;  
        
        return Arrays.copyOf(telefonai, aibesImtis);
    }
    
    // Imamas po vienas elementas iš sugeneruoto masyvo. Kai elementai baigiasi sugeneruojama
    // nuosava situacija ir išmetamas pranešimas.
    public Telefonas parduotiTelefona() {
        if(telefonai == null){
            throw new MyException("phonesNotGenerated");
        }
        if(kiekis < telefonai.length){
            return telefonai[kiekis++];
        } else{
            throw new MyException("allSetStoredToMap");
        }
    }
    
    public String gautiIsBazesTelefonuId(){
        if(raktai == null){
            throw new MyException("phonesIdsNotGenerated");
        }
        if(idKiekis >= raktai.length){
            idKiekis = 0;
        }
        
        return raktai[idKiekis++];
    }
}
