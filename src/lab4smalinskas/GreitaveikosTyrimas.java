package lab4smalinskas;

import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;
import laborai.gui.MyException;
import laborai.studijosktu.HashType;
import laborai.studijosktu.MapKTUx;

public class GreitaveikosTyrimas {
    public static final String FINISH_COMMAND = "finishCommand";
    private static final ResourceBundle MESSAGES = 
            ResourceBundle.getBundle("laborai.gui.messages");
    
    private final BlockingQueue resultsLogger = new SynchronousQueue();
    private final Semaphore semaphore = new Semaphore(-1);
    private final Timekeeper tk;
    
    private final String[] TYRIMU_VARDAI = {"MKTUOAA", "MKTUA", "HMA", 
        "MKTUOAC", "MKTUAC", "HMC", "MKTUOAR", "MKTUAR", "HMR",};
    private final int[] TIRIAMI_KIEKIAI = {10000, 20000, 50000, 100000, 200000};
        
    public GreitaveikosTyrimas(){
        semaphore.release();
        tk = new Timekeeper(TIRIAMI_KIEKIAI, resultsLogger, semaphore);
    }
    
    public void pradetiTyrima(){
        try{
            SisteminisTyrimas();
        } catch(InterruptedException e){
            Thread.currentThread().interrupt();
        } catch(Exception ex){
            ex.printStackTrace(System.out);
        }
    }
    
    public void SisteminisTyrimas() throws InterruptedException{
        try {
            
            for(int k : TIRIAMI_KIEKIAI){
                MapKTUOAx<String, String> map =
                    new MapKTUOAx(new String(), new Telefonas(), 10, 0.75f);
                
                MapKTUx<String, String> map2 =
                    new MapKTUx(new String(), new Telefonas(), 10, 0.75f, 
                        HashType.DIVISION);
                
                HashMap<String, String> map3 = new HashMap();
                
                Telefonas[] telefonuArray = TelefonuGamyba.gamintiTelefonus(k);
                String[] telefonuIdArray = TelefonuGamyba.gamintiTelefonuIds(k);
                map.clear();
                map2.clear();
                tk.startAfterPause();
                tk.start();
                
                for(int i = 0; i < k; i++){
                     map.put(telefonuIdArray[i], telefonuArray[i].toString());
                }
                
                tk.finish(TYRIMU_VARDAI[0]);
                
                for(int i = 0; i < k; i++){
                    map2.put(telefonuIdArray[i], telefonuArray[i].toString());
                }
                
                tk.finish(TYRIMU_VARDAI[1]);
                
                for(int i = 0; i < k; i++){
                    map3.put(telefonuIdArray[i], telefonuArray[i].toString());
                }
                
                tk.finish(TYRIMU_VARDAI[2]);
                
                for(int i = 0; i < k; i++){
                    map.contains(telefonuIdArray[i]);
                }
                
                tk.finish(TYRIMU_VARDAI[3]);
                
                for(int i = 0; i < k; i++){
                    map2.contains(telefonuIdArray[i]);
                }
                
                tk.finish(TYRIMU_VARDAI[4]);
                
                for(int i = 0; i < k; i++){
                    map3.containsKey(telefonuIdArray[i]);
                }
                
                tk.finish(TYRIMU_VARDAI[5]);
                
                for(int i = 0; i < k; i++){
                    map.remove(telefonuIdArray[i]);
                }                
                
                tk.finish(TYRIMU_VARDAI[6]);
                
                for(int i = 0; i < k; i++){
                    map2.remove(telefonuIdArray[i]);
                }
                
                tk.finish(TYRIMU_VARDAI[7]);
                
                for(int i = 0; i < k; i++){
                    map3.remove(telefonuIdArray[i]);
                }
                
                tk.finish(TYRIMU_VARDAI[8]);
                
                tk.seriesFinish();
            }
            
            StringBuilder sb = new StringBuilder();
            tk.logResult(sb.toString());
            tk.logResult(FINISH_COMMAND);
        } catch(MyException e){
            tk.logResult(e.getMessage());
        }
    }
    
    public BlockingQueue<String> getResultsLogger() {
        return resultsLogger;
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }
}
