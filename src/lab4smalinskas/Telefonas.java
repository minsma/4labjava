package lab4smalinskas;

import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;
import laborai.studijosktu.KTUable;
import laborai.studijosktu.Ks;

public final class Telefonas implements KTUable {
    
    // Bendri duomenys visiems telefonams
    private static final int priimtinuIsleidimoMetuRiba = 2012;         // priimtinu telefono isleidimo metu riba
    private static final int esamiMetai = LocalDate.now().getYear();    // kintamasis, kuriame saugojama siu metu reiksme
    private static final double minKaina = 120.0;                       // minimali telefono kaina
    private static final double maxKaina = 1200.0;                      // maksimali telefono kaina
    private String gamintojas = "";                                     // telefono gamintojas
    private String modelis = "";                                        // telefono modelis
    private int isleidMetai = -1;                                       // telefono isleidimo metai
    private double kaina = -1.0;

    public Telefonas() {
    }
    
    public Telefonas(String gamintojas, String modelis, int isleidMetai, 
            double kaina){
        this.gamintojas = gamintojas;
        this.modelis = modelis;
        this.isleidMetai = isleidMetai;
        this.kaina = kaina;
        validate();
    }
    
    public Telefonas(String eilute){
        this.parse(eilute);
    }
    
    public Telefonas(Builder builder){
        this.gamintojas = builder.gamintojas;
        this.modelis = builder.modelis;
        this.isleidMetai = builder.isleidMetai;
        this.kaina = builder.kaina;
        validate();
    }
    
    @Override
    public Telefonas create(String duomenuEilute){
        return new Telefonas(duomenuEilute);
    }
    
    @Override
    public String validate(){
        String klaidosTipas = "";
        
        assert(isleidMetai < priimtinuIsleidimoMetuRiba || isleidMetai > esamiMetai) :
                klaidosTipas = "Blogai nurodyti gamybos metai; ";
        assert(kaina < minKaina || kaina > maxKaina) :
                klaidosTipas = "Kaina uz leistinu ribu; ";
        
        return klaidosTipas;
    }
    
    @Override
    public void parse(String duomenuEilute){
        try { // ed - tai elementarus duomenys, atskirti tarpais
            Scanner ed = new Scanner(duomenuEilute);
            this.gamintojas = ed.next();
            this.modelis = ed.next();
            this.isleidMetai = ed.nextInt();
            this.kaina = ed.nextDouble();            
        } catch(InputMismatchException e){
            Ks.ern("Blogas duomenu formatas apie telefona -> " + duomenuEilute);
        } catch(NoSuchElementException e){
            Ks.ern("Truksta duomenu apie telefona -> " + duomenuEilute);
        }
    }
    
    @Override 
    public String toString(){
        return gamintojas + " " + modelis + " " + isleidMetai + " " + 
                String.format("%4.1f", kaina);
    }

    public String getGamintojas() {
        return gamintojas;
    }

    public void setGamintojas(String gamintojas) {
        this.gamintojas = gamintojas;
    }

    public String getModelis() {
        return modelis;
    }

    public void setModelis(String modelis) {
        this.modelis = modelis;
    }

    public int getIsleidMetai() {
        return isleidMetai;
    }

    public void setIsleidMetai(int isleidMetai) {
        this.isleidMetai = isleidMetai;
    }

    public double getKaina() {
        return kaina;
    }

    public void setKaina(double kaina) {
        this.kaina = kaina;
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(gamintojas, modelis, isleidMetai, kaina);
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null){
            return false;
        }
        
        if(getClass() != obj.getClass()){
            return false;
        }
        
        final Telefonas kitas = (Telefonas) obj;
        
        if(!Objects.equals(this.gamintojas, kitas.gamintojas)){
            return false;
        }
        if(!Objects.equals(this.modelis, kitas.modelis)){
            return false;
        }
        if(!Objects.equals(this.isleidMetai, kitas.isleidMetai)){
            return false;
        }
        if(!Objects.equals(this.kaina, kitas.kaina)){
            return false;
        }
        
        if(Double.doubleToLongBits(this.kaina) != 
                Double.doubleToLongBits(kitas.kaina)){
            return false;
        }
        
        return true;
    }
    
    // Telefonas klases objektu gamintojas
    public static class Builder {
        private final static Random RANDOM = new Random(1949);
        private final static String[][] MODELIAI = {
            {"Iphone", "4", "4s", "5", "5s", "6", "6s", "7", "8", "X"},
            {"LG", "G5", "G6"},
            {"Samsung Galaxy", "S8", "S8+", "NOTE 8", "S7", "S7 Edge", "A3", 
                "A5", "J3", "J3", "J5", "XCOVER 5"},
            {"Sony Xperia", "XZ1", "XZ Premium", "X Compact", "XA", "XZ", "XZS",
                "XA1", "L1", "E5", "M5"},
            {"Huawei", "P10", "P10 Lite", "P10 Plus", "Mate 9 Pro", 
                "P9 Lite 2017", "P9 Lite", "P9 Lite Mini", "Y6", "Nova"} 
        };
        
        private String gamintojas = "";
        private String modelis = "";
        private int isleidMetai = -1;
        private double kaina = -1.0;
        
        public Telefonas build(){
            return new Telefonas(this);
        }
        
        public Telefonas buildRandom(){
            int ma = RANDOM.nextInt(MODELIAI.length);               // gamintojas
            int mo = RANDOM.nextInt(MODELIAI[ma].length - 1) + 1;   // modelis
            
            return new Telefonas(
                    MODELIAI[ma][0],                               // gamintojas
                    MODELIAI[ma][mo],                              // modelis
                    2012 + RANDOM.nextInt(5),                     // metai tarp 2012 ir 2017
                    120.0 + RANDOM.nextDouble() * 1200.0           // kaina tarp 120 ir 1200
            );
        }
        
        public Builder gamintojas(String gamintojas){
            this.gamintojas = gamintojas;
            
            return this;
        }
        
        public Builder modelis(String modelis){
            this.modelis = modelis;
            
            return this;
        }
        
        public Builder isleidMetai(int isleidMetai){
            this.isleidMetai = isleidMetai;
            
            return this;
        }
        
        public Builder kaina(double kaina){
            this.kaina = kaina;
            
            return this;
        }
    }
}
