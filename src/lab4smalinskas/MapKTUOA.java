package lab4smalinskas;

import java.util.Arrays;
import laborai.studijosktu.MapADT;

public class MapKTUOA<K, V> implements MapADT<K, V>{
   
    public static final int DEFAULT_INITIAL_CAPACITY = 16;
    public static final float DEFAULT_LOAD_FACTOR = 0.75f;
    
    // Maisos lentele
    protected Entry<K, V>[] table;
    // Lenteleje esanciu raktas-reiksme poru kiekis
    protected int size = 0;
    // Apkrovimo faktorius
    protected float loadFactor;
    // Einamos poros indeksas maisos lenteleje
    protected int index = 0;
    
    public MapKTUOA(){
        this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR);
    }
    
    public MapKTUOA(int initialCapacity, float loadFactor){
        if(initialCapacity <= 0){
            throw new IllegalArgumentException("Illegal initial capacity: "
                    + initialCapacity);
        }
        
        if((loadFactor <= 0.0) || (loadFactor > 1.0)){
            throw new IllegalArgumentException("Illegal load factor: "
                    + loadFactor);
        }
        
        this.table = new Entry[initialCapacity];
        this.loadFactor = loadFactor;
    }
    
    /**
     * Patikrinama ar atvaizdis yra tuščias.
     *
     * @return true, jei tuščias
     */
    @Override
    public boolean isEmpty(){
        return size == 0;
    }

    /**
     * Grąžinamas atvaizdyje esančių porų kiekis.
     *
     * @return Grąžinamas atvaizdyje esančių porų kiekis.
     */
    @Override
    public int size(){
        return size;
    }

    /**
     * Išvalomas atvaizdis.
     *
     */
    @Override
    public void clear(){
        Arrays.fill(table, null);
        size = 0;
        index = 0;
    }

    @Override
    public String[][] toArray(){
        String[][] result = new String[table.length][];
        int count = 0;
        
        for (Entry<K, V> n : table) {
            String[] list = new String[this.size()];
            
            int countLocal = 0;
            
            if (n != null) {
                list[countLocal++] = n.toString();
            }
            
            result[count] = list;
            count++;
        }
        
        return result;
    }

    /**
     * Atvaizdis papildomas nauja pora.
     *
     * @param key raktas,
     * @param value reikšmė.
     * @return Grąžinama atvaizdžio poros reikšmė.
     */
    @Override
    public V put(K key, V value){
        if(key == null || value == null){
            throw new NullPointerException("Key or value is null in "
                    + "put(Key key, Value value)");
        }
        
        int index = findPosition(key);
        
        if(index == -1){
            rehash();
            put(key, value);
            return value;
        }
        
        if(table[index] == null){
            table[index] = new Entry(key, value);
            size++;
            
            if(size > table.length * loadFactor){
                rehash();
            }
        } else{
            table[index].value = value;
        }
        
        return value;
    }

    /**
     * Grąžinama atvaizdžio poros reikšmė.
     *
     * @param key raktas.
     * @return Grąžinama atvaizdžio poros reikšmė.
     */
    @Override
    public V get(K key){
        if(key == null){
            throw new NullPointerException("Key is null in get(Key key)");
        }
        
        int index = findPosition(key);
        
        return (index != -1 && table[index] != null) ? table[index].value : null;
    }

    /**
     * Iš atvaizdžio pašalinama pora.
     *
     * @param key raktas.
     * @return Grąžinama pašalinta atvaizdžio poros reikšmė.
     */
    @Override
    public V remove(K key){
        if(key == null){
            throw new NullPointerException("Key is null in remove(Key key)");
        }
        
        int index = findPosition(key);
        
        if(index > -1){
            table[index] = null;
            size--;
        }
        
        return null;
    }

    /**
     * Patikrinama ar atvaizdyje egzistuoja pora su raktu key.
     *
     * @param key raktas.
     * @return true, jei atvaizdyje egzistuoja pora su raktu key, kitu atveju -
     * false
     */
    @Override
    public boolean contains(K key){
        return get(key) != null;
    }
    
    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        
        for(Entry<K, V> entry : table){
            if(entry != null){
                result.append(entry.toString()).append(System.lineSeparator());
            }
        }
        
        return result.toString();
    }
    
    private int findPosition(K key){
        int index = hash(key);
        int index0 = index;
        int i = 0;
        
        for(int j = 0; j < table.length; j++){
            if(table[index] == null || table[index].key.equals(key)){
                return index;
            }
            
            i++;
            index = (index0 + index * hash2(key)) % table.length;
        }
        
        return -1;
    }
    
    private int hash(K key){
        int h = key.hashCode();
        
        return Math.abs(h) % table.length;
    }
    
    private int hash2(K key){
        return 7 - (Math.abs(key.hashCode()) % 7);
    }
    
    private void rehash(){
        MapKTUOA mapKTUOA 
                = new MapKTUOA(table.length * 2, loadFactor);
        
        for(int i = 0; i < table.length; i++){
            if(table[i] != null){
                mapKTUOA.put(table[i].key, table[i].value);
            }
        }
        
        table = mapKTUOA.table;
    }
    
    class Entry<K, V> {
        // Raktas
        K key;
        // Reiksme
        V value;
        
        Entry(){
        }
        
        Entry(K key, V value){
            this.key = key;
            this.value = value;
        }
        
        @Override
        public String toString(){
            return key + "=" + value;
        }
    }
}
